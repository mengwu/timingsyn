This archive contains the proof-of-concept implementation as well as all benchmark files to reproduce the experiments for 

**PLDI2019: Abstract Interpretation under Speculative Execution**. 


Getting Started:
====================
Compile from sourcecode
----------
Just compile our tool like other LLVM pass project. It is developed based on LLVM 8.0.1, there may be some API change if other LLCM version is used.
For detials about how to do it, please refer to LLVM document: http://llvm.org/docs/WritingAnLLVMPass.html

If compiled successfully,  the resulting llvm pass will be : `~/llvmbuild/lib/LLVMSpectre.so`  or  `~/llvmbuild/lib/LLVMSpectre.dylib`

The artifact is also available as open format virtual machine in Google Drive: https://goo.gl/Ho1yjq.
The VM contains an ubuntu 14.04 system, login with 

user: artifact

psw: 123

What's included :
-------
 - scripts to reproduce results in the paper: `~/sidechannel.sh`  `~/wcet.sh`
 - source code in directory: `~/llvm-3.9.0.src/lib/Transforms/Spectre/`
 - benchmakrs in directory: `~/bench/`


Run the tool:
------
### Run the ''wcet.sh'' for execution time estimation experiment. (corresponds to table 5 & 6 in the paper)

For example, the following outputs comes from running the wcet/adpcm benchmark.
It is tested for 3 times:

 - run without speculative execution (-specu=0)
 - run with speculative execution, and just-in-time merging option (-specu=1 -merge=0)
 - run with speculative execution, and roll-back merging option (-specu=1 -merge=1), to compare two mergeing strategies.

The tool outputs these statistcs:

 - the number of speculatively executed branches  (column #Branch in Table 5 & 6)
 - the number of total cache misses (column #Miss in Table 5 & 6)
 - the number of speculative cache misses (column #SpMiss in Table 5 & 6)
 - the number of iterations on loop (column #Iteration in Table 5 & 6)
 - the execution time from `time` command is shown as the number before word "user" (column Time in Table 5 & 6). The analysis time may varies in different environment.

### 
```
> time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0  bench/wcet/adpcm.bc -o /dev/null
> time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/wcet/adpcm.bc -o /dev/null
> time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=1 bench/wcet/adpcm.bc -o /dev/null

***wcet/adpcm:
Non-Speculatively Analyze function main
========Num Specu Branch:0======
========Num Cache Misses:24=====
========Num Specu Misses:0======
========Total iterations:0======
========Buff is in cache:0======
0.98user 0.00system 0:00.53elapsed 99%CPU (0avgtext+0avgdata 63928maxresident)k0inputs+0outputs (0major+5018minor)pagefaults 0swaps

Speculatively Analyze function main
  with Just-in-time merging.
========Num Specu Branch:75======
========Num Cache Misses:32=====
========Num Specu Misses:17======
========Total iterations:173======
========Buff is in cache:0======
14.40user 0.03system 0:07.36elapsed 99%CPU (0avgtext+0avgdata 158600maxresident)k0inputs+8outputs (0major+28594minor)pagefaults 0swaps

Speculatively Analyze function main
  with rollback merging.
========Num Specu Branch:75======
========Num Cache Misses:31=====
========Num Specu Misses:25======
========Total iterations:261======
========Buff is in cache:0======
9.98user 0.03system 0:10.25elapsed 99%CPU (0avgtext+0avgdata 197276maxresident)k0inputs+0outputs (0major+38298minor)pagefaults 0swaps
```

### Run the ''sidechannel.sh'' for side channel detection experiment. (corresponds to table 7 in the paper)

For example, the following outputs comes from running the hpn-ssh/hash benchmark.It is tested for both non-speculative mode and speculative mode.

As stated in the paper, for most benchmarks, we manually inserted a user buffer for which we can control its size. The outputs report if this buffer is still in the cache as "Buff is in cache: 1". 
If the last digit is "1", means it is still in the cache, "0" for not in the cache. 

For the hash benchmark, the buffer is reproted to still in the cache, so access it using senstive index is considered to be "safe".
However, under speculative execution, it is already (partially) evicted out of cache. Therefore, access the buffer indexed by sensitive value is not "safe". There are another 4 benchmarks show the difference on leakage detection. The rest of benchmarks are not able to detect such difference, by changing the size the inserted buffer.

### 
```
> time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/hpn-ssh/hash.bc -o /dev/null
> time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/hpn-ssh/hash.bc -o /dev/null

***hpn-ssh/hash:
Non-Speculatively Analyze function crypto_hash_sha512
========Num Specu Branch:0======
========Num Cache Misses:15=====
========Num Specu Misses:0======
========Total iterations:0======
========Buff is in cache:1======
0.66user 0.00system 0:00.67elapsed 98%CPU (0avgtext+0avgdata 61620maxresident)k0inputs+0outputs (0major+5393minor)pagefaults 0swaps

Speculatively Analyze function crypto_hash_sha512
  with Just-in-time merging.
========Num Specu Branch:6======
========Num Cache Misses:16=====
========Num Specu Misses:4======
========Total iterations:13======
========Buff is in cache:0======
1.52user 0.01system 0:01.53elapsed 99%CPU (0avgtext+0avgdata 72468maxresident)k0inputs+8outputs (0major+7981minor)pagefaults 0swaps
```

Have fun!
