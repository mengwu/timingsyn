/*
 * jcmarker.c
 *
 * Copyright (C) 1991-1996, Thomas G. Lane.
 * This file is part of the Independent JPEG Group's software.
 * For conditions of distribution and use, see the accompanying README file.
 *
 * This file contains routines to write JPEG datastream markers.
 */
#include <stdint.h>
#include <stdlib.h> 
#define JPEG_INTERNALS

/* Version ID for the JPEG library.
 * Might be useful for tests like "#if JPEG_LIB_VERSION >= 60".
 */

#define JPEG_LIB_VERSION  61  /* Version 6a */
#define TRUE 1
#define FALSE 0

#define JMETHOD(type,methodname,arglist)  type (*methodname) ()
typedef enum {

#define JMESSAGE(code,string) code ,
JMESSAGE(JMSG_NOMESSAGE, "Bogus message code %d") /* Must be first entry! */

/* For maintenance convenience, list is alphabetical by message code name */
JMESSAGE(JERR_ARITH_NOTIMPL,
   "Sorry, there are legal restrictions on arithmetic coding")
JMESSAGE(JERR_BAD_ALIGN_TYPE, "ALIGN_TYPE is wrong, please fix")
JMESSAGE(JERR_BAD_ALLOC_CHUNK, "MAX_ALLOC_CHUNK is wrong, please fix")
JMESSAGE(JERR_BAD_BUFFER_MODE, "Bogus buffer control mode")
JMESSAGE(JERR_BAD_COMPONENT_ID, "Invalid component ID %d in SOS")
JMESSAGE(JERR_BAD_DCTSIZE, "IDCT output block size %d not supported")
JMESSAGE(JERR_BAD_IN_COLORSPACE, "Bogus input colorspace")
JMESSAGE(JERR_BAD_J_COLORSPACE, "Bogus JPEG colorspace")
JMESSAGE(JERR_BAD_LENGTH, "Bogus marker length")
JMESSAGE(JERR_BAD_LIB_VERSION,
   "Wrong JPEG library version: library is %d, caller expects %d")
JMESSAGE(JERR_BAD_MCU_SIZE, "Sampling factors too large for interleaved scan")
JMESSAGE(JERR_BAD_POOL_ID, "Invalid memory pool code %d")
JMESSAGE(JERR_BAD_PRECISION, "Unsupported JPEG data precision %d")
JMESSAGE(JERR_BAD_PROGRESSION,
   "Invalid progressive parameters Ss=%d Se=%d Ah=%d Al=%d")
JMESSAGE(JERR_BAD_PROG_SCRIPT,
   "Invalid progressive parameters at scan script entry %d")
JMESSAGE(JERR_BAD_SAMPLING, "Bogus sampling factors")
JMESSAGE(JERR_BAD_SCAN_SCRIPT, "Invalid scan script at entry %d")
JMESSAGE(JERR_BAD_STATE, "Improper call to JPEG library in state %d")
JMESSAGE(JERR_BAD_STRUCT_SIZE,
   "JPEG parameter struct mismatch: library thinks size is %u, caller expects %u")
JMESSAGE(JERR_BAD_VIRTUAL_ACCESS, "Bogus virtual array access")
JMESSAGE(JERR_BUFFER_SIZE, "Buffer passed to JPEG library is too small")
JMESSAGE(JERR_CANT_SUSPEND, "Suspension not allowed here")
JMESSAGE(JERR_CCIR601_NOTIMPL, "CCIR601 sampling not implemented yet")
JMESSAGE(JERR_COMPONENT_COUNT, "Too many color components: %d, max %d")
JMESSAGE(JERR_CONVERSION_NOTIMPL, "Unsupported color conversion request")
JMESSAGE(JERR_DAC_INDEX, "Bogus DAC index %d")
JMESSAGE(JERR_DAC_VALUE, "Bogus DAC value 0x%x")
JMESSAGE(JERR_DHT_COUNTS, "Bogus DHT counts")
JMESSAGE(JERR_DHT_INDEX, "Bogus DHT index %d")
JMESSAGE(JERR_DQT_INDEX, "Bogus DQT index %d")
JMESSAGE(JERR_EMPTY_IMAGE, "Empty JPEG image (DNL not supported)")
JMESSAGE(JERR_EMS_READ, "Read from EMS failed")
JMESSAGE(JERR_EMS_WRITE, "Write to EMS failed")
JMESSAGE(JERR_EOI_EXPECTED, "Didn't expect more than one scan")
JMESSAGE(JERR_FILE_READ, "Input file read error")
JMESSAGE(JERR_FILE_WRITE, "Output file write error --- out of disk space?")
JMESSAGE(JERR_FRACT_SAMPLE_NOTIMPL, "Fractional sampling not implemented yet")
JMESSAGE(JERR_HUFF_CLEN_OVERFLOW, "Huffman code size table overflow")
JMESSAGE(JERR_HUFF_MISSING_CODE, "Missing Huffman code table entry")
JMESSAGE(JERR_IMAGE_TOO_BIG, "Maximum supported image dimension is %u pixels")
JMESSAGE(JERR_INPUT_EMPTY, "Empty input file")
JMESSAGE(JERR_INPUT_EOF, "Premature end of input file")
JMESSAGE(JERR_MISMATCHED_QUANT_TABLE,
   "Cannot transcode due to multiple use of quantization table %d")
JMESSAGE(JERR_MISSING_DATA, "Scan script does not transmit all data")
JMESSAGE(JERR_MODE_CHANGE, "Invalid color quantization mode change")
JMESSAGE(JERR_NOTIMPL, "Not implemented yet")
JMESSAGE(JERR_NOT_COMPILED, "Requested feature was omitted at compile time")
JMESSAGE(JERR_NO_BACKING_STORE, "Backing store not supported")
JMESSAGE(JERR_NO_HUFF_TABLE, "Huffman table 0x%02x was not defined")
JMESSAGE(JERR_NO_IMAGE, "JPEG datastream contains no image")
JMESSAGE(JERR_NO_QUANT_TABLE, "Quantization table 0x%02x was not defined")
JMESSAGE(JERR_NO_SOI, "Not a JPEG file: starts with 0x%02x 0x%02x")
JMESSAGE(JERR_OUT_OF_MEMORY, "Insufficient memory (case %d)")
JMESSAGE(JERR_QUANT_COMPONENTS,
   "Cannot quantize more than %d color components")
JMESSAGE(JERR_QUANT_FEW_COLORS, "Cannot quantize to fewer than %d colors")
JMESSAGE(JERR_QUANT_MANY_COLORS, "Cannot quantize to more than %d colors")
JMESSAGE(JERR_SOF_DUPLICATE, "Invalid JPEG file structure: two SOF markers")
JMESSAGE(JERR_SOF_NO_SOS, "Invalid JPEG file structure: missing SOS marker")
JMESSAGE(JERR_SOF_UNSUPPORTED, "Unsupported JPEG process: SOF type 0x%02x")
JMESSAGE(JERR_SOI_DUPLICATE, "Invalid JPEG file structure: two SOI markers")
JMESSAGE(JERR_SOS_NO_SOF, "Invalid JPEG file structure: SOS before SOF")
JMESSAGE(JERR_TFILE_CREATE, "Failed to create temporary file %s")
JMESSAGE(JERR_TFILE_READ, "Read failed on temporary file")
JMESSAGE(JERR_TFILE_SEEK, "Seek failed on temporary file")
JMESSAGE(JERR_TFILE_WRITE,
   "Write failed on temporary file --- out of disk space?")
JMESSAGE(JERR_TOO_LITTLE_DATA, "Application transferred too few scanlines")
JMESSAGE(JERR_UNKNOWN_MARKER, "Unsupported marker type 0x%02x")
JMESSAGE(JERR_VIRTUAL_BUG, "Virtual array controller messed up")
JMESSAGE(JERR_WIDTH_OVERFLOW, "Image too wide for this implementation")
JMESSAGE(JERR_XMS_READ, "Read from XMS failed")
JMESSAGE(JERR_XMS_WRITE, "Write to XMS failed")
JMESSAGE(JMSG_COPYRIGHT, JCOPYRIGHT)
JMESSAGE(JMSG_VERSION, JVERSION)
JMESSAGE(JTRC_16BIT_TABLES,
   "Caution: quantization tables are too coarse for baseline JPEG")
JMESSAGE(JTRC_ADOBE,
   "Adobe APP14 marker: version %d, flags 0x%04x 0x%04x, transform %d")
JMESSAGE(JTRC_APP0, "Unknown APP0 marker (not JFIF), length %u")
JMESSAGE(JTRC_APP14, "Unknown APP14 marker (not Adobe), length %u")
JMESSAGE(JTRC_DAC, "Define Arithmetic Table 0x%02x: 0x%02x")
JMESSAGE(JTRC_DHT, "Define Huffman Table 0x%02x")
JMESSAGE(JTRC_DQT, "Define Quantization Table %d  precision %d")
JMESSAGE(JTRC_DRI, "Define Restart Interval %u")
JMESSAGE(JTRC_EMS_CLOSE, "Freed EMS handle %u")
JMESSAGE(JTRC_EMS_OPEN, "Obtained EMS handle %u")
JMESSAGE(JTRC_EOI, "End Of Image")
JMESSAGE(JTRC_HUFFBITS, "        %3d %3d %3d %3d %3d %3d %3d %3d")
JMESSAGE(JTRC_JFIF, "JFIF APP0 marker, density %dx%d  %d")
JMESSAGE(JTRC_JFIF_BADTHUMBNAILSIZE,
   "Warning: thumbnail image size does not match data length %u")
JMESSAGE(JTRC_JFIF_MINOR, "Unknown JFIF minor revision number %d.%02d")
JMESSAGE(JTRC_JFIF_THUMBNAIL, "    with %d x %d thumbnail image")
JMESSAGE(JTRC_MISC_MARKER, "Skipping marker 0x%02x, length %u")
JMESSAGE(JTRC_PARMLESS_MARKER, "Unexpected marker 0x%02x")
JMESSAGE(JTRC_QUANTVALS, "        %4u %4u %4u %4u %4u %4u %4u %4u")
JMESSAGE(JTRC_QUANT_3_NCOLORS, "Quantizing to %d = %d*%d*%d colors")
JMESSAGE(JTRC_QUANT_NCOLORS, "Quantizing to %d colors")
JMESSAGE(JTRC_QUANT_SELECTED, "Selected %d colors for quantization")
JMESSAGE(JTRC_RECOVERY_ACTION, "At marker 0x%02x, recovery action %d")
JMESSAGE(JTRC_RST, "RST%d")
JMESSAGE(JTRC_SMOOTH_NOTIMPL,
   "Smoothing not supported with nonstandard sampling ratios")
JMESSAGE(JTRC_SOF, "Start Of Frame 0x%02x: width=%u, height=%u, components=%d")
JMESSAGE(JTRC_SOF_COMPONENT, "    Component %d: %dhx%dv q=%d")
JMESSAGE(JTRC_SOI, "Start of Image")
JMESSAGE(JTRC_SOS, "Start Of Scan: %d components")
JMESSAGE(JTRC_SOS_COMPONENT, "    Component %d: dc=%d ac=%d")
JMESSAGE(JTRC_SOS_PARAMS, "  Ss=%d, Se=%d, Ah=%d, Al=%d")
JMESSAGE(JTRC_TFILE_CLOSE, "Closed temporary file %s")
JMESSAGE(JTRC_TFILE_OPEN, "Opened temporary file %s")
JMESSAGE(JTRC_UNKNOWN_IDS,
   "Unrecognized component IDs %d %d %d, assuming YCbCr")
JMESSAGE(JTRC_XMS_CLOSE, "Freed XMS handle %u")
JMESSAGE(JTRC_XMS_OPEN, "Obtained XMS handle %u")
JMESSAGE(JWRN_ADOBE_XFORM, "Unknown Adobe color transform code %d")
JMESSAGE(JWRN_BOGUS_PROGRESSION,
   "Inconsistent progression sequence for component %d coefficient %d")
JMESSAGE(JWRN_EXTRANEOUS_DATA,
   "Corrupt JPEG data: %u extraneous bytes before marker 0x%02x")
JMESSAGE(JWRN_HIT_MARKER, "Corrupt JPEG data: premature end of data segment")
JMESSAGE(JWRN_HUFF_BAD_CODE, "Corrupt JPEG data: bad Huffman code")
JMESSAGE(JWRN_JFIF_MAJOR, "Warning: unknown JFIF revision number %d.%02d")
JMESSAGE(JWRN_JPEG_EOF, "Premature end of JPEG file")
JMESSAGE(JWRN_MUST_RESYNC,
   "Corrupt JPEG data: found marker 0x%02x instead of RST%d")
JMESSAGE(JWRN_NOT_SEQUENTIAL, "Invalid SOS parameters for sequential JPEG")
JMESSAGE(JWRN_TOO_MUCH_DATA, "Application transferred too many scanlines")
} J_MESSAGE_CODE;


/* Fatal errors (print message and exit) */
#define ERREXIT(cinfo,code)  \
  ((cinfo)->err->msg_code = (code), \
   (*(cinfo)->err->error_exit) ((j_common_ptr) (cinfo)))
#define ERREXIT1(cinfo,code,p1)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (*(cinfo)->err->error_exit) ((j_common_ptr) (cinfo)))
#define ERREXIT2(cinfo,code,p1,p2)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (cinfo)->err->msg_parm.i[1] = (p2), \
   (*(cinfo)->err->error_exit) ((j_common_ptr) (cinfo)))
#define ERREXIT3(cinfo,code,p1,p2,p3)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (cinfo)->err->msg_parm.i[1] = (p2), \
   (cinfo)->err->msg_parm.i[2] = (p3), \
   (*(cinfo)->err->error_exit) ((j_common_ptr) (cinfo)))
#define ERREXIT4(cinfo,code,p1,p2,p3,p4)  \
  ((cinfo)->err->msg_code = (code), \
   (cinfo)->err->msg_parm.i[0] = (p1), \
   (cinfo)->err->msg_parm.i[1] = (p2), \
   (cinfo)->err->msg_parm.i[2] = (p3), \
   (cinfo)->err->msg_parm.i[3] = (p4), \
   (*(cinfo)->err->error_exit) ((j_common_ptr) (cinfo)))
#define ERREXITS(cinfo,code,str)  \
  ((cinfo)->err->msg_code = (code), \
   strncpy((cinfo)->err->msg_parm.s, (str), JMSG_STR_PARM_MAX), \
   (*(cinfo)->err->error_exit) ((j_common_ptr) (cinfo)))

#define MAKESTMT(stuff)   do { stuff } while (0)


typedef unsigned char UINT8;
typedef unsigned char JOCTET;

/* UINT16 must hold at least the values 0..65535. */
typedef unsigned short UINT16;
typedef long INT32;

typedef int boolean;
typedef unsigned int JDIMENSION;


struct jvirt_sarray_control { long dummy; };
struct jvirt_barray_control { long dummy; };
struct jpeg_comp_master { long dummy; };
struct jpeg_c_main_controller { long dummy; };
struct jpeg_c_prep_controller { long dummy; };
struct jpeg_c_coef_controller { long dummy; };
struct jpeg_marker_writer { long dummy; };
struct jpeg_color_converter { long dummy; };
struct jpeg_downsampler { long dummy; };
struct jpeg_forward_dct { long dummy; };
struct jpeg_entropy_encoder { long dummy; };
struct jpeg_decomp_master { long dummy; };
struct jpeg_d_main_controller { long dummy; };
struct jpeg_d_coef_controller { long dummy; };
struct jpeg_d_post_controller { long dummy; };
struct jpeg_input_controller { long dummy; };
struct jpeg_marker_reader { long dummy; };
struct jpeg_entropy_decoder { long dummy; };
struct jpeg_inverse_dct { long dummy; };
struct jpeg_upsampler { long dummy; };
struct jpeg_color_deconverter { long dummy; };
struct jpeg_color_quantizer { long dummy; };

/* Various constants determining the sizes of things.
 * All of these are specified by the JPEG standard, so don't change them
 * if you want to be compatible.
 */

#define DCTSIZE       8 /* The basic DCT block is 8x8 samples */
#define DCTSIZE2      64  /* DCTSIZE squared; # of elements in a block */
#define NUM_QUANT_TBLS      4 /* Quantization tables are numbered 0..3 */
#define NUM_HUFF_TBLS       4 /* Huffman tables are numbered 0..3 */
#define NUM_ARITH_TBLS      16  /* Arith-coding tables are numbered 0..15 */
#define MAX_COMPS_IN_SCAN   4 /* JPEG limit on # of components in one scan */
#define MAX_SAMP_FACTOR     4 /* JPEG limit on sampling factors */
/* Unfortunately, some bozo at Adobe saw no reason to be bound by the standard;
 * the PostScript DCT filter can emit files with many more than 10 blocks/MCU.
 * If you happen to run across such a file, you can up D_MAX_BLOCKS_IN_MCU
 * to handle it.  We even let you do this from the jconfig.h file.  However,
 * we strongly discourage changing C_MAX_BLOCKS_IN_MCU; just because Adobe
 * sometimes emits noncompliant files doesn't mean you should too.
 */
#define C_MAX_BLOCKS_IN_MCU   10 /* compressor's limit on blocks per MCU */
#ifndef D_MAX_BLOCKS_IN_MCU
#define D_MAX_BLOCKS_IN_MCU   10 /* decompressor's limit on blocks per MCU */
#endif

/*
 * jpeg_natural_order[i] is the natural-order position of the i'th element
 * of zigzag order.
 *
 * When reading corrupted data, the Huffman decoders could attempt
 * to reference an entry beyond the end of this array (if the decoded
 * zero run length reaches past the end of the block).  To prevent
 * wild stores without adding an inner-loop test, we put some extra
 * "63"s after the real entries.  This will cause the extra coefficient
 * to be stored in location 63 of the block, not somewhere random.
 * The worst case would be a run-length of 15, which means we need 16
 * fake entries.
 */

const int jpeg_natural_order[DCTSIZE2+16] = {
  0,  1,  8, 16,  9,  2,  3, 10,
 17, 24, 32, 25, 18, 11,  4,  5,
 12, 19, 26, 33, 40, 48, 41, 34,
 27, 20, 13,  6,  7, 14, 21, 28,
 35, 42, 49, 56, 57, 50, 43, 36,
 29, 22, 15, 23, 30, 37, 44, 51,
 58, 59, 52, 45, 38, 31, 39, 46,
 53, 60, 61, 54, 47, 55, 62, 63,
 63, 63, 63, 63, 63, 63, 63, 63, /* extra entries for safety in decoder */
 63, 63, 63, 63, 63, 63, 63, 63
};
/* Data structures for images (arrays of samples and of DCT coefficients).
 * On 80x86 machines, the image arrays are too big for near pointers,
 * but the pointer arrays can fit in near memory.
 */

// typedef JSAMPLE FAR *JSAMPROW;  /* ptr to one image row of pixel samples. */
// typedef JSAMPROW *JSAMPARRAY; /* ptr to some rows (a 2-D sample array) */
// typedef JSAMPARRAY *JSAMPIMAGE; /* a 3-D sample array: top index is color */

// typedef JCOEF JBLOCK[DCTSIZE2]; /* one block of coefficients */
// typedef JBLOCK FAR *JBLOCKROW;  /* pointer to one row of coefficient blocks */
// typedef JBLOCKROW *JBLOCKARRAY;   /* a 2-D array of coefficient blocks */
// typedef JBLOCKARRAY *JBLOCKIMAGE; /* a 3-D array of coefficient blocks */

// typedef JCOEF FAR *JCOEFPTR;  /* useful in a couple of places */


/* Types for JPEG compression parameters and working tables. */


/* DCT coefficient quantization tables. */

typedef struct {
  /* This array gives the coefficient quantizers in natural array order
   * (not the zigzag order in which they are stored in a JPEG DQT marker).
   * CAUTION: IJG versions prior to v6a kept this array in zigzag order.
   */
  UINT16 quantval[DCTSIZE2];  /* quantization step for each coefficient */
  /* This field is used only during compression.  It's initialized FALSE when
   * the table is created, and set TRUE when it's been output to the file.
   * You could suppress output of a table by setting this to TRUE.
   * (See jpeg_suppress_tables for an example.)
   */
  boolean sent_table;   /* TRUE when table has been output */
} JQUANT_TBL;


/* Huffman coding tables. */

typedef struct {
  /* These two fields directly represent the contents of a JPEG DHT marker */
  UINT8 bits[17];   /* bits[k] = # of symbols with codes of */
        /* length k bits; bits[0] is unused */
  UINT8 huffval[256];   /* The symbols, in order of incr code length */
  /* This field is used only during compression.  It's initialized FALSE when
   * the table is created, and set TRUE when it's been output to the file.
   * You could suppress output of a table by setting this to TRUE.
   * (See jpeg_suppress_tables for an example.)
   */
  boolean sent_table;   /* TRUE when table has been output */
} JHUFF_TBL;


/* Data destination object for compression */

struct jpeg_destination_mgr {
  JOCTET * next_output_byte;  /* => next byte to write in buffer */
  size_t free_in_buffer;  /* # of byte spaces remaining in buffer */

  JMETHOD(void, init_destination, (j_compress_ptr cinfo));
  JMETHOD(boolean, empty_output_buffer, (j_compress_ptr cinfo));
  JMETHOD(void, term_destination, (j_compress_ptr cinfo));
};



/* Basic info about one component (color channel). */

typedef struct {
  /* These values are fixed over the whole image. */
  /* For compression, they must be supplied by parameter setup; */
  /* for decompression, they are read from the SOF marker. */
  int component_id;   /* identifier for this component (0..255) */
  int component_index;    /* its index in SOF or cinfo->comp_info[] */
  int h_samp_factor;    /* horizontal sampling factor (1..4) */
  int v_samp_factor;    /* vertical sampling factor (1..4) */
  int quant_tbl_no;   /* quantization table selector (0..3) */
  /* These values may vary between scans. */
  /* For compression, they must be supplied by parameter setup; */
  /* for decompression, they are read from the SOS marker. */
  /* The decompressor output side may not use these variables. */
  int dc_tbl_no;    /* DC entropy table selector (0..3) */
  int ac_tbl_no;    /* AC entropy table selector (0..3) */
  
  /* Remaining fields should be treated as private by applications. */
  
  /* These values are computed during compression or decompression startup: */
  /* Component's size in DCT blocks.
   * Any dummy blocks added to complete an MCU are not counted; therefore
   * these values do not depend on whether a scan is interleaved or not.
   */
  JDIMENSION width_in_blocks;
  JDIMENSION height_in_blocks;
  /* Size of a DCT block in samples.  Always DCTSIZE for compression.
   * For decompression this is the size of the output from one DCT block,
   * reflecting any scaling we choose to apply during the IDCT step.
   * Values of 1,2,4,8 are likely to be supported.  Note that different
   * components may receive different IDCT scalings.
   */
  int DCT_scaled_size;
  /* The downsampled dimensions are the component's actual, unpadded number
   * of samples at the main buffer (preprocessing/compression interface), thus
   * downsampled_width = ceil(image_width * Hi/Hmax)
   * and similarly for height.  For decompression, IDCT scaling is included, so
   * downsampled_width = ceil(image_width * Hi/Hmax * DCT_scaled_size/DCTSIZE)
   */
  JDIMENSION downsampled_width;  /* actual width in samples */
  JDIMENSION downsampled_height; /* actual height in samples */
  /* This flag is used only for decompression.  In cases where some of the
   * components will be ignored (eg grayscale output from YCbCr image),
   * we can skip most computations for the unused components.
   */
  boolean component_needed; /* do we need the value of this component? */

  /* These values are computed before starting a scan of the component. */
  /* The decompressor output side may not use these variables. */
  int MCU_width;    /* number of blocks per MCU, horizontally */
  int MCU_height;   /* number of blocks per MCU, vertically */
  int MCU_blocks;   /* MCU_width * MCU_height */
  int MCU_sample_width;   /* MCU width in samples, MCU_width*DCT_scaled_size */
  int last_col_width;   /* # of non-dummy blocks across in last MCU */
  int last_row_height;    /* # of non-dummy blocks down in last MCU */

  /* Saved quantization table for component; NULL if none yet saved.
   * See jdinput.c comments about the need for this information.
   * This field is currently used only for decompression.
   */
  JQUANT_TBL * quant_table;

  /* Private per-component storage for DCT or IDCT subsystem. */
  void * dct_table;
} jpeg_component_info;

/* The script for encoding a multiple-scan file is an array of these: */

typedef struct {
  int comps_in_scan;    /* number of components encoded in this scan */
  int component_index[MAX_COMPS_IN_SCAN]; /* their SOF/comp_info[] indexes */
  int Ss, Se;     /* progressive JPEG spectral selection parms */
  int Ah, Al;     /* progressive JPEG successive approx. parms */
} jpeg_scan_info;


/* Known color spaces. */

typedef enum {
  JCS_UNKNOWN,    /* error/unspecified */
  JCS_GRAYSCALE,    /* monochrome */
  JCS_RGB,    /* red/green/blue */
  JCS_YCbCr,    /* Y/Cb/Cr (also known as YUV) */
  JCS_CMYK,   /* C/M/Y/K */
  JCS_YCCK    /* Y/Cb/Cr/K */
} J_COLOR_SPACE;

/* DCT/IDCT algorithm options. */

typedef enum {
  JDCT_ISLOW,   /* slow but accurate integer algorithm */
  JDCT_IFAST,   /* faster, less accurate integer method */
  JDCT_FLOAT    /* floating-point: accurate, fast on fast HW */
} J_DCT_METHOD;

#ifndef JDCT_DEFAULT    /* may be overridden in jconfig.h */
#define JDCT_DEFAULT  JDCT_ISLOW
#endif
#ifndef JDCT_FASTEST    /* may be overridden in jconfig.h */
#define JDCT_FASTEST  JDCT_IFAST
#endif

/* Dithering options for decompression. */

typedef enum {
  JDITHER_NONE,   /* no dithering */
  JDITHER_ORDERED,  /* simple ordered dither */
  JDITHER_FS    /* Floyd-Steinberg error diffusion dither */
} J_DITHER_MODE;


/* Common fields between JPEG compression and decompression master structs. */

#define jpeg_common_fields \
  struct jpeg_error_mgr * err;  /* Error handler module */\
  struct jpeg_memory_mgr * mem; /* Memory manager module */\
  struct jpeg_progress_mgr * progress; /* Progress monitor, or NULL if none */\
  boolean is_decompressor;  /* so common code can tell which is which */\
  int global_state    /* for checking call sequence validity */

/* Routines that are to be used by both halves of the library are declared
 * to receive a pointer to this structure.  There are no actual instances of
 * jpeg_common_struct, only of jpeg_compress_struct and jpeg_decompress_struct.
 */
struct jpeg_common_struct {
  jpeg_common_fields;   /* Fields common to both master struct types */
  /* Additional fields follow in an actual jpeg_compress_struct or
   * jpeg_decompress_struct.  All three structs must agree on these
   * initial fields!  (This would be a lot cleaner in C++.)
   */
};

typedef struct jpeg_common_struct * j_common_ptr;
typedef struct jpeg_compress_struct * j_compress_ptr;
// typedef struct jpeg_decompress_struct * j_decompress_ptr;


/* Master record for a compression instance */

struct jpeg_compress_struct {
  jpeg_common_fields;   /* Fields shared with jpeg_decompress_struct */

  /* Destination for compressed data */
  struct jpeg_destination_mgr * dest;

  /* Description of source image --- these fields must be filled in by
   * outer application before starting compression.  in_color_space must
   * be correct before you can even call jpeg_set_defaults().
   */

  JDIMENSION image_width; /* input image width */
  JDIMENSION image_height;  /* input image height */
  int input_components;   /* # of color components in input image */
  J_COLOR_SPACE in_color_space; /* colorspace of input image */

  double input_gamma;   /* image gamma of input image */

  /* Compression parameters --- these fields must be set before calling
   * jpeg_start_compress().  We recommend calling jpeg_set_defaults() to
   * initialize everything to reasonable defaults, then changing anything
   * the application specifically wants to change.  That way you won't get
   * burnt when new parameters are added.  Also note that there are several
   * helper routines to simplify changing parameters.
   */

  int data_precision;   /* bits of precision in image data */

  int num_components;   /* # of color components in JPEG image */
  J_COLOR_SPACE jpeg_color_space; /* colorspace of JPEG image */

  jpeg_component_info * comp_info;
  /* comp_info[i] describes component that appears i'th in SOF */
  
  JQUANT_TBL * quant_tbl_ptrs[NUM_QUANT_TBLS];
  /* ptrs to coefficient quantization tables, or NULL if not defined */
  
  JHUFF_TBL * dc_huff_tbl_ptrs[NUM_HUFF_TBLS];
  JHUFF_TBL * ac_huff_tbl_ptrs[NUM_HUFF_TBLS];
  /* ptrs to Huffman coding tables, or NULL if not defined */
  
  UINT8 arith_dc_L[NUM_ARITH_TBLS]; /* L values for DC arith-coding tables */
  UINT8 arith_dc_U[NUM_ARITH_TBLS]; /* U values for DC arith-coding tables */
  UINT8 arith_ac_K[NUM_ARITH_TBLS]; /* Kx values for AC arith-coding tables */

  int num_scans;    /* # of entries in scan_info array */
  const jpeg_scan_info * scan_info; /* script for multi-scan file, or NULL */
  /* The default value of scan_info is NULL, which causes a single-scan
   * sequential JPEG file to be emitted.  To create a multi-scan file,
   * set num_scans and scan_info to point to an array of scan definitions.
   */

  boolean raw_data_in;    /* TRUE=caller supplies downsampled data */
  boolean arith_code;   /* TRUE=arithmetic coding, FALSE=Huffman */
  boolean optimize_coding;  /* TRUE=optimize entropy encoding parms */
  boolean CCIR601_sampling; /* TRUE=first samples are cosited */
  int smoothing_factor;   /* 1..100, or 0 for no input smoothing */
  J_DCT_METHOD dct_method;  /* DCT algorithm selector */

  /* The restart interval can be specified in absolute MCUs by setting
   * restart_interval, or in MCU rows by setting restart_in_rows
   * (in which case the correct restart_interval will be figured
   * for each scan).
   */
  unsigned int restart_interval; /* MCUs per restart, or 0 for no restart */
  int restart_in_rows;    /* if > 0, MCU rows per restart interval */

  /* Parameters controlling emission of special markers. */

  boolean write_JFIF_header;  /* should a JFIF marker be written? */
  /* These three values are not used by the JPEG code, merely copied */
  /* into the JFIF APP0 marker.  density_unit can be 0 for unknown, */
  /* 1 for dots/inch, or 2 for dots/cm.  Note that the pixel aspect */
  /* ratio is defined by X_density/Y_density even when density_unit=0. */
  UINT8 density_unit;   /* JFIF code for pixel size units */
  UINT16 X_density;   /* Horizontal pixel density */
  UINT16 Y_density;   /* Vertical pixel density */
  boolean write_Adobe_marker; /* should an Adobe marker be written? */
  
  /* State variable: index of next scanline to be written to
   * jpeg_write_scanlines().  Application may use this to control its
   * processing loop, e.g., "while (next_scanline < image_height)".
   */

  JDIMENSION next_scanline; /* 0 .. image_height-1  */

  /* Remaining fields are known throughout compressor, but generally
   * should not be touched by a surrounding application.
   */

  /*
   * These fields are computed during compression startup
   */
  boolean progressive_mode; /* TRUE if scan script uses progressive mode */
  int max_h_samp_factor;  /* largest h_samp_factor */
  int max_v_samp_factor;  /* largest v_samp_factor */

  JDIMENSION total_iMCU_rows; /* # of iMCU rows to be input to coef ctlr */
  /* The coefficient controller receives data in units of MCU rows as defined
   * for fully interleaved scans (whether the JPEG file is interleaved or not).
   * There are v_samp_factor * DCTSIZE sample rows of each component in an
   * "iMCU" (interleaved MCU) row.
   */
  
  /*
   * These fields are valid during any one scan.
   * They describe the components and MCUs actually appearing in the scan.
   */
  int comps_in_scan;    /* # of JPEG components in this scan */
  jpeg_component_info * cur_comp_info[MAX_COMPS_IN_SCAN];
  /* *cur_comp_info[i] describes component that appears i'th in SOS */
  
  JDIMENSION MCUs_per_row;  /* # of MCUs across the image */
  JDIMENSION MCU_rows_in_scan;  /* # of MCU rows in the image */
  
  int blocks_in_MCU;    /* # of DCT blocks per MCU */
  int MCU_membership[C_MAX_BLOCKS_IN_MCU];
  /* MCU_membership[i] is index in cur_comp_info of component owning */
  /* i'th block in an MCU */

  int Ss, Se, Ah, Al;   /* progressive JPEG parameters for scan */

  /*
   * Links to compression subobjects (methods and private variables of modules)
   */
  struct jpeg_comp_master * master;
  struct jpeg_c_main_controller * main;
  struct jpeg_c_prep_controller * prep;
  struct jpeg_c_coef_controller * coef;
  struct jpeg_marker_writer * marker;
  struct jpeg_color_converter * cconvert;
  struct jpeg_downsampler * downsample;
  struct jpeg_forward_dct * fdct;
  struct jpeg_entropy_encoder * entropy;
};


/* Master record for a decompression instance */

// struct jpeg_decompress_struct {
//   jpeg_common_fields;   /* Fields shared with jpeg_compress_struct */

//   /* Source of compressed data */
//   struct jpeg_source_mgr * src;

//   /* Basic description of image --- filled in by jpeg_read_header(). */
//   /* Application may inspect these values to decide how to process image. */

//   JDIMENSION image_width; /* nominal image width (from SOF marker) */
//   JDIMENSION image_height;  /* nominal image height */
//   int num_components;   /* # of color components in JPEG image */
//   J_COLOR_SPACE jpeg_color_space; /* colorspace of JPEG image */

//   /* Decompression processing parameters --- these fields must be set before
//    * calling jpeg_start_decompress().  Note that jpeg_read_header() initializes
//    * them to default values.
//    */

//   J_COLOR_SPACE out_color_space; /* colorspace for output */

//   unsigned int scale_num, scale_denom; /* fraction by which to scale image */

//   double output_gamma;    /* image gamma wanted in output */

//   boolean buffered_image; /* TRUE=multiple output passes */
//   boolean raw_data_out;   /* TRUE=downsampled data wanted */

//   J_DCT_METHOD dct_method;  /* IDCT algorithm selector */
//   boolean do_fancy_upsampling;  /* TRUE=apply fancy upsampling */
//   boolean do_block_smoothing; /* TRUE=apply interblock smoothing */

//   boolean quantize_colors;  /* TRUE=colormapped output wanted */
//   /* the following are ignored if not quantize_colors: */
//   J_DITHER_MODE dither_mode;  /* type of color dithering to use */
//   boolean two_pass_quantize;  /* TRUE=use two-pass color quantization */
//   int desired_number_of_colors; /* max # colors to use in created colormap */
//   /* these are significant only in buffered-image mode: */
//   boolean enable_1pass_quant; /* enable future use of 1-pass quantizer */
//   boolean enable_external_quant;/* enable future use of external colormap */
//   boolean enable_2pass_quant; /* enable future use of 2-pass quantizer */

//   /* Description of actual output image that will be returned to application.
//    * These fields are computed by jpeg_start_decompress().
//    * You can also use jpeg_calc_output_dimensions() to determine these values
//    * in advance of calling jpeg_start_decompress().
//    */

//   JDIMENSION output_width;  /* scaled image width */
//   JDIMENSION output_height; /* scaled image height */
//   int out_color_components; /* # of color components in out_color_space */
//   int output_components;  /* # of color components returned */
//   /* output_components is 1 (a colormap index) when quantizing colors;
//    * otherwise it equals out_color_components.
//    */
//   int rec_outbuf_height;  /* min recommended height of scanline buffer */
//   /* If the buffer passed to jpeg_read_scanlines() is less than this many rows
//    * high, space and time will be wasted due to unnecessary data copying.
//    * Usually rec_outbuf_height will be 1 or 2, at most 4.
//    */

//   /* When quantizing colors, the output colormap is described by these fields.
//    * The application can supply a colormap by setting colormap non-NULL before
//    * calling jpeg_start_decompress; otherwise a colormap is created during
//    * jpeg_start_decompress or jpeg_start_output.
//    * The map has out_color_components rows and actual_number_of_colors columns.
//    */
//   int actual_number_of_colors;  /* number of entries in use */
//   JSAMPARRAY colormap;    /* The color map as a 2-D pixel array */

//   /* State variables: these variables indicate the progress of decompression.
//    * The application may examine these but must not modify them.
//    */

//   /* Row index of next scanline to be read from jpeg_read_scanlines().
//    * Application may use this to control its processing loop, e.g.,
//    * "while (output_scanline < output_height)".
//    */
//   JDIMENSION output_scanline; /* 0 .. output_height-1  */

//   /* Current input scan number and number of iMCU rows completed in scan.
//    * These indicate the progress of the decompressor input side.
//    */
//   int input_scan_number;  /* Number of SOS markers seen so far */
//   JDIMENSION input_iMCU_row;  /* Number of iMCU rows completed */

//   /* The "output scan number" is the notional scan being displayed by the
//    * output side.  The decompressor will not allow output scan/row number
//    * to get ahead of input scan/row, but it can fall arbitrarily far behind.
//    */
//   int output_scan_number; /* Nominal scan number being displayed */
//   JDIMENSION output_iMCU_row; /* Number of iMCU rows read */

//    Current progression status.  coef_bits[c][i] indicates the precision
//    * with which component c's DCT coefficient i (in zigzag order) is known.
//    * It is -1 when no data has yet been received, otherwise it is the point
//    * transform (shift) value for the most recent scan of the coefficient
//    * (thus, 0 at completion of the progression).
//    * This pointer is NULL when reading a non-progressive file.
   
//   int (*coef_bits)[DCTSIZE2]; /* -1 or current Al value for each coef */

//   /* Internal JPEG parameters --- the application usually need not look at
//    * these fields.  Note that the decompressor output side may not use
//    * any parameters that can change between scans.
//    */

//   /* Quantization and Huffman tables are carried forward across input
//    * datastreams when processing abbreviated JPEG datastreams.
//    */

//   JQUANT_TBL * quant_tbl_ptrs[NUM_QUANT_TBLS];
//   /* ptrs to coefficient quantization tables, or NULL if not defined */

//   JHUFF_TBL * dc_huff_tbl_ptrs[NUM_HUFF_TBLS];
//   JHUFF_TBL * ac_huff_tbl_ptrs[NUM_HUFF_TBLS];
//   /* ptrs to Huffman coding tables, or NULL if not defined */

//   /* These parameters are never carried across datastreams, since they
//    * are given in SOF/SOS markers or defined to be reset by SOI.
//    */

//   int data_precision;   /* bits of precision in image data */

//   jpeg_component_info * comp_info;
//   /* comp_info[i] describes component that appears i'th in SOF */

//   boolean progressive_mode; /* TRUE if SOFn specifies progressive mode */
//   boolean arith_code;   /* TRUE=arithmetic coding, FALSE=Huffman */

//   UINT8 arith_dc_L[NUM_ARITH_TBLS]; /* L values for DC arith-coding tables */
//   UINT8 arith_dc_U[NUM_ARITH_TBLS]; /* U values for DC arith-coding tables */
//   UINT8 arith_ac_K[NUM_ARITH_TBLS]; /* Kx values for AC arith-coding tables */

//   unsigned int restart_interval; /* MCUs per restart interval, or 0 for no restart */

//   /* These fields record data obtained from optional markers recognized by
//    * the JPEG library.
//    */
//   boolean saw_JFIF_marker;  /* TRUE iff a JFIF APP0 marker was found */
//   /* Data copied from JFIF marker: */
//   UINT8 density_unit;   /* JFIF code for pixel size units */
//   UINT16 X_density;   /* Horizontal pixel density */
//   UINT16 Y_density;   /* Vertical pixel density */
//   boolean saw_Adobe_marker; /* TRUE iff an Adobe APP14 marker was found */
//   UINT8 Adobe_transform;  /* Color transform code from Adobe marker */

//   boolean CCIR601_sampling; /* TRUE=first samples are cosited */

//   /* Remaining fields are known throughout decompressor, but generally
//    * should not be touched by a surrounding application.
//    */

//   /*
//    * These fields are computed during decompression startup
//    */
//   int max_h_samp_factor;  /* largest h_samp_factor */
//   int max_v_samp_factor;  /* largest v_samp_factor */

//   int min_DCT_scaled_size;  /* smallest DCT_scaled_size of any component */

//   JDIMENSION total_iMCU_rows; /* # of iMCU rows in image */
//   /* The coefficient controller's input and output progress is measured in
//    * units of "iMCU" (interleaved MCU) rows.  These are the same as MCU rows
//    * in fully interleaved JPEG scans, but are used whether the scan is
//    * interleaved or not.  We define an iMCU row as v_samp_factor DCT block
//    * rows of each component.  Therefore, the IDCT output contains
//    * v_samp_factor*DCT_scaled_size sample rows of a component per iMCU row.
//    */

//   JSAMPLE * sample_range_limit; /* table for fast range-limiting */

//   /*
//    * These fields are valid during any one scan.
//    * They describe the components and MCUs actually appearing in the scan.
//    * Note that the decompressor output side must not use these fields.
//    */
//   int comps_in_scan;    /* # of JPEG components in this scan */
//   jpeg_component_info * cur_comp_info[MAX_COMPS_IN_SCAN];
//   /* *cur_comp_info[i] describes component that appears i'th in SOS */

//   JDIMENSION MCUs_per_row;  /* # of MCUs across the image */
//   JDIMENSION MCU_rows_in_scan;  /* # of MCU rows in the image */

//   int blocks_in_MCU;    /* # of DCT blocks per MCU */
//   int MCU_membership[D_MAX_BLOCKS_IN_MCU];
//   /* MCU_membership[i] is index in cur_comp_info of component owning */
//   /* i'th block in an MCU */

//   int Ss, Se, Ah, Al;   /* progressive JPEG parameters for scan */

//   /* This field is shared between entropy decoder and marker parser.
//    * It is either zero or the code of a JPEG marker that has been
//    * read from the data source, but has not yet been processed.
//    */
//   int unread_marker;

//   /*
//    * Links to decompression subobjects (methods, private variables of modules)
//    */
//   struct jpeg_decomp_master * master;
//   struct jpeg_d_main_controller * main;
//   struct jpeg_d_coef_controller * coef;
//   struct jpeg_d_post_controller * post;
//   struct jpeg_input_controller * inputctl;
//   struct jpeg_marker_reader * marker;
//   struct jpeg_entropy_decoder * entropy;
//   struct jpeg_inverse_dct * idct;
//   struct jpeg_upsampler * upsample;
//   struct jpeg_color_deconverter * cconvert;
//   struct jpeg_color_quantizer * cquantize;
// };



typedef enum {			/* JPEG marker codes */
  M_SOF0  = 0xc0,
  M_SOF1  = 0xc1,
  M_SOF2  = 0xc2,
  M_SOF3  = 0xc3,
  
  M_SOF5  = 0xc5,
  M_SOF6  = 0xc6,
  M_SOF7  = 0xc7,
  
  M_JPG   = 0xc8,
  M_SOF9  = 0xc9,
  M_SOF10 = 0xca,
  M_SOF11 = 0xcb,
  
  M_SOF13 = 0xcd,
  M_SOF14 = 0xce,
  M_SOF15 = 0xcf,
  
  M_DHT   = 0xc4,
  
  M_DAC   = 0xcc,
  
  M_RST0  = 0xd0,
  M_RST1  = 0xd1,
  M_RST2  = 0xd2,
  M_RST3  = 0xd3,
  M_RST4  = 0xd4,
  M_RST5  = 0xd5,
  M_RST6  = 0xd6,
  M_RST7  = 0xd7,
  
  M_SOI   = 0xd8,
  M_EOI   = 0xd9,
  M_SOS   = 0xda,
  M_DQT   = 0xdb,
  M_DNL   = 0xdc,
  M_DRI   = 0xdd,
  M_DHP   = 0xde,
  M_EXP   = 0xdf,
  
  M_APP0  = 0xe0,
  M_APP1  = 0xe1,
  M_APP2  = 0xe2,
  M_APP3  = 0xe3,
  M_APP4  = 0xe4,
  M_APP5  = 0xe5,
  M_APP6  = 0xe6,
  M_APP7  = 0xe7,
  M_APP8  = 0xe8,
  M_APP9  = 0xe9,
  M_APP10 = 0xea,
  M_APP11 = 0xeb,
  M_APP12 = 0xec,
  M_APP13 = 0xed,
  M_APP14 = 0xee,
  M_APP15 = 0xef,
  
  M_JPG0  = 0xf0,
  M_JPG13 = 0xfd,
  M_COM   = 0xfe,
  
  M_TEM   = 0x01,
  
  M_ERROR = 0x100
} JPEG_MARKER;


/*
 * Basic output routines.
 *
 * Note that we do not support suspension while writing a marker.
 * Therefore, an application using suspension must ensure that there is
 * enough buffer space for the initial markers (typ. 600-700 bytes) before
 * calling jpeg_start_compress, and enough space to write the trailing EOI
 * (a few bytes) before calling jpeg_finish_compress.  Multipass compression
 * modes are not supported at all with suspension, so those two are the only
 * points where markers will be written.
 */

inline void emit_byte (j_compress_ptr cinfo, int val) __attribute__((always_inline))
/* Emit a byte */
{
  struct jpeg_destination_mgr * dest = cinfo->dest;

  *(dest->next_output_byte)++ = (JOCTET) val;
  // if (--dest->free_in_buffer == 0) {
  //   if (! (*dest->empty_output_buffer) (cinfo))
  //     ERREXIT(cinfo, JERR_CANT_SUSPEND);
  // }
}


inline void
emit_marker (j_compress_ptr cinfo, JPEG_MARKER mark)  __attribute__((always_inline))
/* Emit a marker code */
{
  emit_byte(cinfo, 0xFF);
  emit_byte(cinfo, (int) mark);
}


inline void
emit_2bytes (j_compress_ptr cinfo, int value) __attribute__((always_inline))
/* Emit a 2-byte integer; these are always MSB first in JPEG files */
{
  emit_byte(cinfo, (value >> 8) & 0xFF);
  emit_byte(cinfo, value & 0xFF);
}


/*
 * Routines to write specific marker types.
 */

inline int
emit_dqt (j_compress_ptr cinfo, int index) __attribute__((always_inline))
/* Emit a DQT marker */
/* Returns the precision used (0 = 8bits, 1 = 16bits) for baseline checking */
{
  JQUANT_TBL * qtbl = cinfo->quant_tbl_ptrs[index];
  int prec;
  int i;

  // if (qtbl == NULL)
  //   ERREXIT1(cinfo, JERR_NO_QUANT_TABLE, index);

  prec = 0;
  for (i = 0; i < DCTSIZE2; i++) {
    if (qtbl->quantval[i] > 255)
      prec = 1;
  }

  if (! qtbl->sent_table) {
    emit_marker(cinfo, M_DQT);

    emit_2bytes(cinfo, prec ? DCTSIZE2*2 + 1 + 2 : DCTSIZE2 + 1 + 2);

    emit_byte(cinfo, index + (prec<<4));

    for (i = 0; i < DCTSIZE2; i++) {
      /* The table entries must be emitted in zigzag order. */
      unsigned int qval = qtbl->quantval[jpeg_natural_order[i]];
      if (prec)
	emit_byte(cinfo, qval >> 8);
      emit_byte(cinfo, qval & 0xFF);
    }

    qtbl->sent_table = TRUE;
  }

  return prec;
}


inline void emit_dht (j_compress_ptr cinfo, int index, boolean is_ac) __attribute__((always_inline))
/* Emit a DHT marker */
{
  JHUFF_TBL * htbl;
  int length, i;
  
  if (is_ac) {
    htbl = cinfo->ac_huff_tbl_ptrs[index];
    index += 0x10;		/* output index has AC bit set */
  } else {
    htbl = cinfo->dc_huff_tbl_ptrs[index];
  }

  // if (htbl == NULL)
  //   ERREXIT1(cinfo, JERR_NO_HUFF_TABLE, index);
  
  if (! htbl->sent_table) {
    emit_marker(cinfo, M_DHT);
    
    length = 0;
    for (i = 1; i <= 16; i++)
      length += htbl->bits[i];
    
    emit_2bytes(cinfo, length + 2 + 1 + 16);
    emit_byte(cinfo, index);
    
    for (i = 1; i <= 16; i++)
      emit_byte(cinfo, htbl->bits[i]);
    
    for (i = 0; i < length; i++)
      emit_byte(cinfo, htbl->huffval[i]);
    
    htbl->sent_table = TRUE;
  }
}


inline void emit_dac (j_compress_ptr cinfo)  __attribute__((always_inline))
/* Emit a DAC marker */
/* Since the useful info is so small, we want to emit all the tables in */
/* one DAC marker.  Therefore this routine does its own scan of the table. */
{
// #ifdef C_ARITH_CODING_SUPPORTED
  char dc_in_use[NUM_ARITH_TBLS];
  char ac_in_use[NUM_ARITH_TBLS];
  int length, i;
  jpeg_component_info *compptr;
  
  for (i = 0; i < NUM_ARITH_TBLS; i++)
    dc_in_use[i] = ac_in_use[i] = 0;
  
  for (i = 0; i < cinfo->comps_in_scan; i++) {
    compptr = cinfo->cur_comp_info[i];
    dc_in_use[compptr->dc_tbl_no] = 1;
    ac_in_use[compptr->ac_tbl_no] = 1;
  }
  
  length = 0;
  for (i = 0; i < NUM_ARITH_TBLS; i++)
    length += dc_in_use[i] + ac_in_use[i];
  
  emit_marker(cinfo, M_DAC);
  
  emit_2bytes(cinfo, length*2 + 2);
  
  for (i = 0; i < NUM_ARITH_TBLS; i++) {
    if (dc_in_use[i]) {
      emit_byte(cinfo, i);
      emit_byte(cinfo, cinfo->arith_dc_L[i] + (cinfo->arith_dc_U[i]<<4));
    }
    if (ac_in_use[i]) {
      emit_byte(cinfo, i + 0x10);
      emit_byte(cinfo, cinfo->arith_ac_K[i]);
    }
  }
// #endif /* C_ARITH_CODING_SUPPORTED */
}


inline void
emit_dri (j_compress_ptr cinfo) __attribute__((always_inline))
/* Emit a DRI marker */
{
  emit_marker(cinfo, M_DRI);
  
  emit_2bytes(cinfo, 4);	/* fixed length */

  emit_2bytes(cinfo, (int) cinfo->restart_interval);
}


inline void
emit_sof (j_compress_ptr cinfo, JPEG_MARKER code) __attribute__((always_inline))
/* Emit a SOF marker */
{
  int ci;
  jpeg_component_info *compptr;
  
  emit_marker(cinfo, code);
  
  emit_2bytes(cinfo, 3 * cinfo->num_components + 2 + 5 + 1); /* length */

  /* Make sure image isn't bigger than SOF field can handle */
  // if ((long) cinfo->image_height > 65535L ||
  //     (long) cinfo->image_width > 65535L)
  //   ERREXIT1(cinfo, JERR_IMAGE_TOO_BIG, (unsigned int) 65535);

  emit_byte(cinfo, cinfo->data_precision);
  emit_2bytes(cinfo, (int) cinfo->image_height);
  emit_2bytes(cinfo, (int) cinfo->image_width);

  emit_byte(cinfo, cinfo->num_components);

  for (ci = 0, compptr = cinfo->comp_info; ci < cinfo->num_components;
       ci++, compptr++) {
    emit_byte(cinfo, compptr->component_id);
    emit_byte(cinfo, (compptr->h_samp_factor << 4) + compptr->v_samp_factor);
    emit_byte(cinfo, compptr->quant_tbl_no);
  }
}


inline void
emit_sos (j_compress_ptr cinfo) __attribute__((always_inline))
/* Emit a SOS marker */
{
  int i, td, ta;
  jpeg_component_info *compptr;
  
  emit_marker(cinfo, M_SOS);
  
  emit_2bytes(cinfo, 2 * cinfo->comps_in_scan + 2 + 1 + 3); /* length */
  
  emit_byte(cinfo, cinfo->comps_in_scan);
  
  for (i = 0; i < cinfo->comps_in_scan; i++) {
    compptr = cinfo->cur_comp_info[i];
    emit_byte(cinfo, compptr->component_id);
    td = compptr->dc_tbl_no;
    ta = compptr->ac_tbl_no;
    if (cinfo->progressive_mode) {
      /* Progressive mode: only DC or only AC tables are used in one scan;
       * furthermore, Huffman coding of DC refinement uses no table at all.
       * We emit 0 for unused field(s); this is recommended by the P&M text
       * but does not seem to be specified in the standard.
       */
      if (cinfo->Ss == 0) {
	ta = 0;			/* DC scan */
	if (cinfo->Ah != 0 && !cinfo->arith_code)
	  td = 0;		/* no DC table either */
      } else {
	td = 0;			/* AC scan */
      }
    }
    emit_byte(cinfo, (td << 4) + ta);
  }

  emit_byte(cinfo, cinfo->Ss);
  emit_byte(cinfo, cinfo->Se);
  emit_byte(cinfo, (cinfo->Ah << 4) + cinfo->Al);
}


void
emit_jfif_app0 (j_compress_ptr cinfo)
/* Emit a JFIF-compliant APP0 marker */
{
  /*
   * Length of APP0 block	(2 bytes)
   * Block ID			(4 bytes - ASCII "JFIF")
   * Zero byte			(1 byte to terminate the ID string)
   * Version Major, Minor	(2 bytes - 0x01, 0x01)
   * Units			(1 byte - 0x00 = none, 0x01 = inch, 0x02 = cm)
   * Xdpu			(2 bytes - dots per unit horizontal)
   * Ydpu			(2 bytes - dots per unit vertical)
   * Thumbnail X size		(1 byte)
   * Thumbnail Y size		(1 byte)
   */
  
  emit_marker(cinfo, M_APP0);
  
  emit_2bytes(cinfo, 2 + 4 + 1 + 2 + 1 + 2 + 2 + 1 + 1); /* length */

  emit_byte(cinfo, 0x4A);	/* Identifier: ASCII "JFIF" */
  emit_byte(cinfo, 0x46);
  emit_byte(cinfo, 0x49);
  emit_byte(cinfo, 0x46);
  emit_byte(cinfo, 0);
  /* We currently emit version code 1.01 since we use no 1.02 features.
   * This may avoid complaints from some older decoders.
   */
  emit_byte(cinfo, 1);		/* Major version */
  emit_byte(cinfo, 1);		/* Minor version */
  emit_byte(cinfo, cinfo->density_unit); /* Pixel size information */
  emit_2bytes(cinfo, (int) cinfo->X_density);
  emit_2bytes(cinfo, (int) cinfo->Y_density);
  emit_byte(cinfo, 0);		/* No thumbnail image */
  emit_byte(cinfo, 0);
}


void
emit_adobe_app14 (j_compress_ptr cinfo)
/* Emit an Adobe APP14 marker */
{
  /*
   * Length of APP14 block	(2 bytes)
   * Block ID			(5 bytes - ASCII "Adobe")
   * Version Number		(2 bytes - currently 100)
   * Flags0			(2 bytes - currently 0)
   * Flags1			(2 bytes - currently 0)
   * Color transform		(1 byte)
   *
   * Although Adobe TN 5116 mentions Version = 101, all the Adobe files
   * now in circulation seem to use Version = 100, so that's what we write.
   *
   * We write the color transform byte as 1 if the JPEG color space is
   * YCbCr, 2 if it's YCCK, 0 otherwise.  Adobe's definition has to do with
   * whether the encoder performed a transformation, which is pretty useless.
   */
  
  emit_marker(cinfo, M_APP14);
  
  emit_2bytes(cinfo, 2 + 5 + 2 + 2 + 2 + 1); /* length */

  emit_byte(cinfo, 0x41);	/* Identifier: ASCII "Adobe" */
  emit_byte(cinfo, 0x64);
  emit_byte(cinfo, 0x6F);
  emit_byte(cinfo, 0x62);
  emit_byte(cinfo, 0x65);
  emit_2bytes(cinfo, 100);	/* Version */
  emit_2bytes(cinfo, 0);	/* Flags0 */
  emit_2bytes(cinfo, 0);	/* Flags1 */
  switch (cinfo->jpeg_color_space) {
  case JCS_YCbCr:
    emit_byte(cinfo, 1);	/* Color transform = 1 */
    break;
  case JCS_YCCK:
    emit_byte(cinfo, 2);	/* Color transform = 2 */
    break;
  default:
    emit_byte(cinfo, 0);	/* Color transform = 0 */
    break;
  }
}


/*
 * This routine is exported for possible use by applications.
 * The intended use is to emit COM or APPn markers after calling
 * jpeg_start_compress() and before the first jpeg_write_scanlines() call
 * (hence, after write_file_header but before write_frame_header).
 * Other uses are not guaranteed to produce desirable results.
 */

void
write_any_marker (j_compress_ptr cinfo, int marker,
		  const JOCTET *dataptr, unsigned int datalen)
/* Emit an arbitrary marker with parameters */
{
  if (datalen <= (unsigned int) 65533) { /* safety check */
    emit_marker(cinfo, (JPEG_MARKER) marker);
  
    emit_2bytes(cinfo, (int) (datalen + 2)); /* total length */

    while (datalen--) {
      emit_byte(cinfo, *dataptr);
      dataptr++;
    }
  }
}


/*
 * Write datastream header.
 * This consists of an SOI and optional APPn markers.
 * We recommend use of the JFIF marker, but not the Adobe marker,
 * when using YCbCr or grayscale data.  The JFIF marker should NOT
 * be used for any other JPEG colorspace.  The Adobe marker is helpful
 * to distinguish RGB, CMYK, and YCCK colorspaces.
 * Note that an application can write additional header markers after
 * jpeg_start_compress returns.
 */

void
write_file_header (j_compress_ptr cinfo)
{
  emit_marker(cinfo, M_SOI);	/* first the SOI */

  if (cinfo->write_JFIF_header)	/* next an optional JFIF APP0 */
    emit_jfif_app0(cinfo);
  if (cinfo->write_Adobe_marker) /* next an optional Adobe APP14 */
    emit_adobe_app14(cinfo);
}


/*
 * Write frame header.
 * This consists of DQT and SOFn markers.
 * Note that we do not emit the SOF until we have emitted the DQT(s).
 * This avoids compatibility problems with incorrect implementations that
 * try to error-check the quant table numbers as soon as they see the SOF.
 */

// void write_frame_header (j_compress_ptr cinfo)
// {
//   int ci, prec;
//   boolean is_baseline;
//   jpeg_component_info *compptr;
  
//   /* Emit DQT for each quantization table.
//    * Note that emit_dqt() suppresses any duplicate tables.
//    */
//   prec = 0;
//   for (ci = 0, compptr = cinfo->comp_info; ci < cinfo->num_components;
//        ci++, compptr++) {
//     prec += emit_dqt(cinfo, compptr->quant_tbl_no);
//   }
//   /* now prec is nonzero iff there are any 16-bit quant tables. */

//   /* Check for a non-baseline specification.
//    * Note we assume that Huffman table numbers won't be changed later.
//    */
//   if (cinfo->arith_code || cinfo->progressive_mode ||
//       cinfo->data_precision != 8) {
//     is_baseline = FALSE;
//   } else {
//     is_baseline = TRUE;
//     for (ci = 0, compptr = cinfo->comp_info; ci < cinfo->num_components;
// 	 ci++, compptr++) {
//       if (compptr->dc_tbl_no > 1 || compptr->ac_tbl_no > 1)
// 	is_baseline = FALSE;
//     }
//     if (prec && is_baseline) {
//       is_baseline = FALSE;
//       /* If it's baseline except for quantizer size, warn the user */
//       // TRACEMS(cinfo, 0, JTRC_16BIT_TABLES);
//     }
//   }

//   /* Emit the proper SOF marker */
//   if (cinfo->arith_code) {
//     emit_sof(cinfo, M_SOF9);	/* SOF code for arithmetic coding */
//   } else {
//     if (cinfo->progressive_mode)
//       emit_sof(cinfo, M_SOF2);	/* SOF code for progressive Huffman */
//     else if (is_baseline)
//       emit_sof(cinfo, M_SOF0);	/* SOF code for baseline implementation */
//     else
//       emit_sof(cinfo, M_SOF1);	/* SOF code for non-baseline Huffman file */
//   }
// }


/*
 * Write scan header.
 * This consists of DHT or DAC markers, optional DRI, and SOS.
 * Compressed data will be written following the SOS.
 */

struct jpeg_compress_struct cinfoSt;


void write_scan_header ()  __attribute__((annotate("specuAnaly")))
{
  int i;
  jpeg_component_info *compptr;
  j_compress_ptr cinfo = &cinfoSt;

  if (cinfo->arith_code) {
    /* Emit arith conditioning info.  We may have some duplication
     * if the file has multiple scans, but it's so small it's hardly
     * worth worrying about.
     */
    emit_dac(cinfo);
  } else {
    /* Emit Huffman tables.
     * Note that emit_dht() suppresses any duplicate tables.
     */
    for (i = 0; i < cinfo->comps_in_scan; i++) {
      compptr = cinfo->cur_comp_info[i];
      if (cinfo->progressive_mode) {
	/* Progressive mode: only DC or only AC tables are used in one scan */
	if (cinfo->Ss == 0) {
	  if (cinfo->Ah == 0)	/* DC needs no table for refinement scan */
	    emit_dht(cinfo, compptr->dc_tbl_no, FALSE);
	} else {
	  emit_dht(cinfo, compptr->ac_tbl_no, TRUE);
	}
      } else {
	/* Sequential mode: need both DC and AC tables */
	emit_dht(cinfo, compptr->dc_tbl_no, FALSE);
	emit_dht(cinfo, compptr->ac_tbl_no, TRUE);
      }
    }
  }

  /* Emit DRI if required --- note that DRI value could change for each scan.
   * If it doesn't, a tiny amount of space is wasted in multiple-scan files.
   * We assume DRI will never be nonzero for one scan and zero for a later one.
   */
  if (cinfo->restart_interval)
    emit_dri(cinfo);

  emit_sos(cinfo);
}


// /*
//  * Write datastream trailer.
//  */

// void
// write_file_trailer (j_compress_ptr cinfo)
// {
//   emit_marker(cinfo, M_EOI);
// }


// /*
//  * Write an abbreviated table-specification datastream.
//  * This consists of SOI, DQT and DHT tables, and EOI.
//  * Any table that is defined and not marked sent_table = TRUE will be
//  * emitted.  Note that all tables will be marked sent_table = TRUE at exit.
//  */

// void
// write_tables_only (j_compress_ptr cinfo)
// {
//   int i;

//   emit_marker(cinfo, M_SOI);

//   for (i = 0; i < NUM_QUANT_TBLS; i++) {
//     if (cinfo->quant_tbl_ptrs[i] != NULL)
//       (void) emit_dqt(cinfo, i);
//   }

//   if (! cinfo->arith_code) {
//     for (i = 0; i < NUM_HUFF_TBLS; i++) {
//       if (cinfo->dc_huff_tbl_ptrs[i] != NULL)
// 	emit_dht(cinfo, i, FALSE);
//       if (cinfo->ac_huff_tbl_ptrs[i] != NULL)
// 	emit_dht(cinfo, i, TRUE);
//     }
//   }

//   emit_marker(cinfo, M_EOI);
// }


// /*
//  * Initialize the marker writer module.
//  */

// void
// jinit_marker_writer (j_compress_ptr cinfo)
// {
//   /* Create the subobject */
//   cinfo->marker = (struct jpeg_marker_writer *)
//     (*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_IMAGE,
// 				SIZEOF(struct jpeg_marker_writer));
//   /* Initialize method pointers */
//   cinfo->marker->write_any_marker = write_any_marker;
//   cinfo->marker->write_file_header = write_file_header;
//   cinfo->marker->write_frame_header = write_frame_header;
//   cinfo->marker->write_scan_header = write_scan_header;
//   cinfo->marker->write_file_trailer = write_file_trailer;
//   cinfo->marker->write_tables_only = write_tables_only;
// }
