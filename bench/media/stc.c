#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h> 

typedef unsigned char   byte; /*  8 bit */
typedef unsigned short  word16;  /* 16 bit */

typedef unsigned int  stc_pixel;
typedef unsigned int  uint;

/* Define the type for gray or RGB values at the driver interface. */
typedef unsigned short gx_color_value;

#define STC_BYTE    8 /* Pass Bytes  to the Dithering-Routine */
#define STC_LONG   16 /* Pass Longs  to the Dithering-Routine */
#define STC_FLOAT  24 /* Pass Floats to the Dithering-Routine */
#define STC_TYPE   24 /* all the type-bits */

#define STC_CMYK10 32 /* Special 32-Bit CMYK-Coding */
#define STC_DIRECT 64 /* Suppress conversion of Scanlines */
#define STC_WHITE 128 /* Call Algorithm for white lines too (out == NULL) */
#define STC_SCAN  256 /* multiply by number of scanlines in buffer */


#define STCDFLAG0  0x000001L /* Algorithm-Bit 0 */
#define STCDFLAG1  0x000002L /* Algorithm-Bit 1 */
#define STCDFLAG2  0x000004L /* Algorithm-Bit 2 */
#define STCDFLAG3  0x000008L /* Algorithm-Bit 3 */
#define STCDFLAG4  0x000010L /* Algorithm-Bit 4 */
#define STCCMYK10  0x000020L /* CMYK10-Coding active */

#define STCUNIDIR  0x000040L /* Unidirectional, if set */
#define STCUWEAVE  0x000080L /* Hardware Microweave */
#define STCNWEAVE  0x000100L /* Software Microweave disabled */

#define STCOK4GO   0x000200L /* stc_put_params was o.k. */

#define STCCOMP    0x000C00L /* RLE, Plain (>= 1.18) */
#define STCPLAIN   0x000400L /* No compression */
#define STCDELTA   0x000800L /* Delta-Row */

#define STCMODEL   0x00f000L /* STC, ST800 */
#define STCST800   0x001000L /* Monochrome-Variant */
#define STCSTCII   0x002000L /* Stylus Color II */

#define STCBAND    0x010000L /* Initialization defined */
#define STCHEIGHT  0x020000L /* Page-Length set */
#define STCWIDTH   0x040000L /* Page-Length set */
#define STCTOP     0x080000L /* Top-Margin set */
#define STCBOTTOM  0x100000L /* Bottom-Margin set */
#define STCINIT    0x200000L /* Initialization defined */
#define STCRELEASE 0x400000L /* Release defined */

#define STCPRINT   0x800000L /* Data printed */

/*
 * Color-Values for the output
 */
#define BLACK   1 /* in monochrome-Mode as well as in CMYK-Mode */
#define RED     4 /* in RGB-Mode */
#define GREEN   2
#define BLUE    1
#define CYAN    8 /* in CMYK-Mode */
#define MAGENTA 4
#define YELLOW  2

#define _param_array_struct(sname,etype)\
  struct sname { const etype *data; uint size; bool persistent; }
typedef _param_array_struct(gs_param_string_s, byte) gs_param_string;
typedef _param_array_struct(gs_param_int_array_s, int) gs_param_int_array;
typedef _param_array_struct(gs_param_float_array_s, float) gs_param_float_array;
typedef _param_array_struct(gs_param_string_array_s, gs_param_string) gs_param_string_array;

/*
 * Define the parameters controlling banding.
 */
typedef struct gx_band_params_s {
  int BandWidth;        /* (optional) band width in pixels */
  int BandHeight;       /* (optional) */
  long BandBufferSpace;       /* (optional) */
} gx_band_params;
#define band_params_initial_values 0, 0, 0

/* Structure for generic printer devices. */
/* This must be preceded by gx_device_common. */
/* Printer devices are actually a union of a memory device */
/* and a clist device, plus some additional state. */
#define prn_fname_sizeof 80
typedef struct gdev_prn_space_params_s {
   long MaxBitmap;         /* max size of non-buffered bitmap */
   long BufferSpace;    /* space to use for buffer */
   gx_band_params band;    /* see gxclist.h */
} gdev_prn_space_params;

/*** Datatype for the array of dithering-Algorithms ***/

#define stc_proc_dither(name) \
 int name(P5(stcolor_device *sdev,int npixel,byte *in,byte *buf,byte *out))

typedef struct stc_dither_s {
  const char *name; /* Mode-Name for Dithering */
  // stc_proc_dither((*fun));
  uint        flags;
  uint        bufadd;
  double      minmax[2];
} stc_dither_t;

/*** Auxillary-Device Structure ***/

typedef struct stc_s {
   long            flags;      /* some mode-flags */
   int             bits;       /* the number of bits per component */
   const struct stc_dither_s  *dither;     /* dithering-mode */
   float          *am;         /* 3/9/16-E. vector/matrix */

   float          *extc[4];    /* Given arrays for stccode */
   uint            sizc[4];    /* Size of extcode-arrays */
   gx_color_value *code[4];    /* cv -> internal */

   float          *extv[4];    /* Given arrays for stcvals */
   uint            sizv[4];    /* Size of extvals-arrays */
   byte           *vals[4];    /* internal -> dithering */

   stc_pixel  white_run[3];    /* the white-pattern */
   stc_pixel  white_end[3];    /* the white-Trailer */
   gs_param_string_array
                   algorithms; /* Names of the available algorithms */

   gs_param_string escp_init;     /* Initialization-Sequence */
   gs_param_string escp_release;  /* Initialization-Sequence */
   int             escp_width; /* Number of Pixels printed */
   int             escp_height;/* Height send to the Printer */
   int             escp_top;   /* Top-Margin, send to the printer */
   int             escp_bottom;/* Bottom-Margin, send to the printer */

   int             alg_item;   /* Size of the items used by the algorithm */

   int             prt_buf;    /* Number of buffers */
   int             prt_size;   /* Size of the Printer-buffer */
   int             escp_size;  /* Size of the ESC/P2-buffer */
   int             seed_size;  /* Size of the seed-buffers */

   int             escp_u;     /* print-resolution (3600 / ydpi )*/
   int             escp_c;     /* selected color */
   int             escp_v;     /* spacing within band */
   int             escp_h;     /* 3600 / xdpi */
   int             escp_m;     /* number of heads */
   int             escp_lf;    /* linefeed in units */

   int             prt_y;      /* print-coordinate */
   int             stc_y;      /* Next line 2b printed */
   int             buf_y;      /* Next line 2b loaded into the buffer */
   int             prt_scans;  /* number of lines printed */


   int            *prt_width;  /* Width of buffered lines */
   byte          **prt_data;   /* Buffered printer-lines */
   byte           *escp_data;  /* Buffer for ESC/P2-Data */
   byte           *seed_row[4];/* Buffer for delta-row compression (prt_size) */

} stc_t;



#define gx_prn_device_common\
      /* ------ Device parameters that must be set ------ */\
      /* ------ before calling the device open routine. ------ */\
   gdev_prn_space_params space_params;\
   char fname[prn_fname_sizeof]; /* OutputFile */\
      /* ------ Other device parameters ------ */\
   int NumCopies;\
     bool NumCopies_set;\
   bool OpenOutputFile;\
   bool Duplex;\
     int Duplex_set;    /* -1 = not supported */\
      /* ------ End of parameters ------ */\
   bool file_is_new;    /* true iff file just opened */\
   FILE *file;       /* output file */\
   long buffer_space;   /* amount of space for clist buffer, */\
               /* 0 means not using clist */\
   byte *buf;        /* buffer for rendering */
   // gx_device_procs orig_procs /* original (std_)procs */


/* ---------------- Device structure ---------------- */

/*
 * Define the generic device structure.  The device procedures can
 * have two different configurations:
 * 
 * - Statically initialized devices predating release 2.8.1
 * set the static_procs pointer to point to a separate procedure record,
 * and do not initialize std_procs.
 *
 * - Statically initialized devices starting with release 2.8.1,
 * and all dynamically created device instances,
 * set the static_procs pointer to 0, and initialize std_procs.
 *
 * The gx_device_set_procs procedure converts the first of these to
 * the second, which is what all client code starting in 2.8.1 expects
 * (using the std_procs record, not the static_procs pointer, to call the
 * driver procedures).
 *
 * The choice of the name Margins (rather than, say, HWOffset), and the
 * specification in terms of a default device resolution rather than
 * 1/72" units, are due to Adobe.
 *
 * ****** NOTE: If you define any subclasses of gx_device, you *must* define
 * ****** the finalization procedure as gx_device_finalize.  Finalization
 * ****** procedures are not automatically inherited.
 */
/* Define the structure for device color capabilities. */
typedef struct gx_device_color_info_s {
   int num_components;     /* 1 = gray only, 3 = RGB, */
               /* 4 = CMYK */
   int depth;        /* # of bits per pixel */
   gx_color_value max_gray;   /* # of distinct gray levels -1 */
   gx_color_value max_color;  /* # of distinct color levels -1 */
               /* (only relevant if num_comp. > 1) */
   gx_color_value dither_grays;  /* size of gray ramp for dithering */
   gx_color_value dither_colors; /* size of color cube ditto */
               /* (only relevant if num_comp. > 1) */
} gx_device_color_info;


#define gx_device_common\
   int params_size;     /* OBSOLETE if stype != 0: */\
               /* size of this structure */\
   const char *dname;      /* the device name */\
               /* 0 iff static prototype */\
   bool is_open;        /* true if device has been opened */\
   int max_fill_band;      /* limit on band size for fill, */\
               /* must be 0 or a power of 2 */\
               /* (see gdevabuf.c for more info) */\
   gx_device_color_info color_info; /* color information */\
   int width;        /* width in pixels */\
   int height;       /* height in pixels */\
   float MediaSize[2];     /* media dimensions in points */\
   float ImagingBBox[4];      /* imageable region in points */\
     bool ImagingBBox_set;\
   float HWResolution[2];     /* resolution, dots per inch */\
   float MarginsHWResolution[2]; /* resolution for Margins */\
   float Margins[2];    /* offset of physical page corner */\
               /* from device coordinate (0,0), */\
               /* in units given by MarginsHWResolution */\
   float HWMargins[4];     /* margins around imageable area, */\
               /* in default user units ("points") */\
   long PageCount;         /* number of pages written */\
   long ShowpageCount;     /* number of calls on showpage */\
   bool IgnoreNumCopies;      /* if true, force num_copies = 1 */
   // gx_page_device_procs page_procs; /* must be last */\
   //     end of std_device_body \
   // gx_device_procs std_procs  /* standard procedures */

#define x_pixels_per_inch HWResolution[0]
#define y_pixels_per_inch HWResolution[1]


/* The device descriptor */
struct gx_device_printer_s {
   gx_device_common;
   gx_prn_device_common;
};




/*** Main-Device Structure ***/

typedef struct stcolor_device_s {
   gx_device_common;
   gx_prn_device_common;
        stc_t stc;
} stcolor_device;

/*
 * The following is an algorithm under test
 */
inline int stc_hscmyk(stcolor_device *sdev,int npixel,byte *in,byte *buf,byte *out) __attribute__((always_inline))
{

/* ============================================================= */
   if(npixel < 0) {  /* npixel <= 0 -> initialisation            */
/* ============================================================= */

      int i,i2do;
      long *lp = (long *) buf;

/* CMYK-only algorithm */
      if( sdev->color_info.num_components != 4)                      return -1;

/*
 * check wether stcdither & TYPE are correct
 */
      if(( sdev->stc.dither                    == NULL) ||
         ((sdev->stc.dither->flags & STC_TYPE) != STC_LONG))         return -2;

/*
 * check wether the buffer-size is sufficiently large
 */
      if(((sdev->stc.dither->flags/STC_SCAN) < 1) ||
         ( sdev->stc.dither->bufadd          <
          (1 + 2*sdev->color_info.num_components)))                  return -3;

/*
 * must have STC_CMYK10, STC_DIRECT, but not STC_WHITE
 */
      if((sdev->stc.dither->flags & STC_CMYK10) == 0)                return -4;
      if((sdev->stc.dither->flags & STC_DIRECT) == 0)                return -5;
      if((sdev->stc.dither->flags & STC_WHITE ) != 0)                return -6;

/*
 * Must have values between 0-1023.0
 */
      if((sdev->stc.dither->minmax[0] !=    0.0) ||
         (sdev->stc.dither->minmax[1] != 1023.0))                    return -7;
/*
 * initialize buffer
 */

     i2do            = 1 + 8 - 4 * npixel;
     lp[0] = 0;

      if(sdev->stc.flags & STCDFLAG0) {
        for(i = 1; i < i2do; ++i) lp[i] = 0;
      } else {
        for(i = 1; i < i2do; ++i)  lp[i] = (rand() % 381) - 190;
      }

/* ============================================================= */
   } else {  /* npixel > 0 && in != NULL  -> scanline-processing */
/* ============================================================= */

      long errc[4],*errv;
      int             step  = buf[0] ? -1 : 1;
      stc_pixel *ip    =  (stc_pixel *) in;

      buf[0] = ~ buf[0];
      errv   =  (long *) buf + 5;

      if(step < 0) {
        ip   += npixel-1;
        out  += npixel-1;
        errv += 4*(npixel-1);
      }

      errc[0] = 0; errc[1] = 0; errc[2] = 0; errc[3] = 0;

      while(npixel-- > 0) {

         register  stc_pixel ci,mode;
         register  long           k,v,n;
         register  int pixel; /* internal pixel-value */

         ci      = *ip; ip += step;

         mode    = ci & 3;
         k       = (ci>>2) & 0x3ff;
         pixel   = 0;

         v       = k+errv[3]+((7*errc[3])>>4);

         if(mode == 3) { /* only Black allowed to fire */

            if(v > 511) {
               v     -= 1023;
               pixel  = BLACK;
            }
            errv[3-(step<<2)] += ((3*v+8)>>4);        /* 3/16 */
            errv[3]            = ((5*v+errc[3]+8)>>4);/* 5/16 +1/16 (rest) */
            errc[3]            = v;

            errv[0] = errv[0] < -190 ? -190 : errv[0] < 190 ? errv[0] : 190;
            errv[1] = errv[1] < -190 ? -190 : errv[1] < 190 ? errv[1] : 190;
            errv[2] = errv[2] < -190 ? -190 : errv[2] < 190 ? errv[2] : 190;

            errc[0] = 0; errc[1] = 0; errc[2] = 0;

         } else if(v > 511) { /* black known to fire */

            v    -= 1023;
            pixel = BLACK;

            errv[3-(step<<2)] += ((3*v+8)>>4);        /* 3/16 */
            errv[3]            = ((5*v+errc[3]+8)>>4);/* 5/16 +1/16 (rest) */
            errc[3]            = v;

            n = (ci>>12) & 0x3ff;

            if(mode == 2) { v = k; }
            else          { v = n; n = (ci>>22) & 0x3ff; }

            v += errv[2]+((7*errc[2])>>4)-1023;
            if(v < -511) v = -511;
            errv[2-(step<<2)] += ((3*v+8)>>4);        /* 3/16 */
            errv[2]            = ((5*v+errc[2]+8)>>4);/* 5/16 +1/16 (rest) */
            errc[2]            = v;

            if(mode == 1) { v = k; }
            else          { v = n; n = (ci>>22) & 0x3ff; }

            v += errv[1]+((7*errc[1])>>4)-1023;
            if(v < -511) v = -511;
            errv[1-(step<<2)] += ((3*v+8)>>4);        /* 3/16 */
            errv[1]            = ((5*v+errc[1]+8)>>4);/* 5/16 +1/16 (rest) */
            errc[1]            = v;

            if(mode == 0) v = k;
            else          v = n;

            v += errv[0]+((7*errc[0])>>4)-1023;
            if(v < -511) v = -511;
            errv[0-(step<<2)] += ((3*v+8)>>4);        /* 3/16 */
            errv[0]            = ((5*v+errc[0]+8)>>4);/* 5/16 +1/16 (rest) */
            errc[0]            = v;

         } else { /* Black does not fire initially */

            long kv = v; /* Black computed after colors */

            n = (ci>>12) & 0x3ff;

            if(mode == 2) { v = k; }
            else          { v = n; n = (ci>>22) & 0x3ff; }

            v += errv[2]+((7*errc[2])>>4);
            if(v > 511) {
               pixel |= YELLOW;
               v     -= 1023;
            }
            errv[2-(step<<2)] += ((3*v+8)>>4);        /* 3/16 */
            errv[2]            = ((5*v+errc[2]+8)>>4);/* 5/16 +1/16 (rest) */
            errc[2]            = v;

            if(mode == 1) { v = k; }
            else          { v = n; n = (ci>>22) & 0x3ff; }

            v += errv[1]+((7*errc[1])>>4);
            if(v > 511) {
               pixel |= MAGENTA;
               v     -= 1023;
            }
            errv[1-(step<<2)] += ((3*v+8)>>4);        /* 3/16 */
            errv[1]            = ((5*v+errc[1]+8)>>4);/* 5/16 +1/16 (rest) */
            errc[1]            = v;

            if(mode == 0) v = k;
            else          v = n;

            v += errv[0]+((7*errc[0])>>4);
            if(v > 511) {
               pixel |= CYAN;
               v     -= 1023;
            }
            errv[0-(step<<2)] += ((3*v+8)>>4);        /* 3/16 */
            errv[0]            = ((5*v+errc[0]+8)>>4);/* 5/16 +1/16 (rest) */
            errc[0]            = v;

            v = kv;
            if(pixel == (CYAN|MAGENTA|YELLOW)) {
               pixel = BLACK;
               v     = v > 511 ? v-1023 : -511;
            }
            errv[3-(step<<2)] += ((3*v+8)>>4);        /* 3/16 */
            errv[3]            = ((5*v+errc[3]+8)>>4);/* 5/16 +1/16 (rest) */
            errc[3]            = v;

         }

         errv += step<<2;
         *out  = pixel; out += step;

      }                                         /* loop over pixels */

/* ============================================================= */
   } /* initialisation, white or scanline-processing             */
/* ============================================================= */

   return 0;
}


int main() __attribute__((annotate("specuAnaly")))
{
   stcolor_device sdev;
   int npixel;
   byte in[1024];
   byte buf[1024];
   byte out[1024];

   stc_hscmyk(&sdev, npixel, in, buf,out);
}