#testing script for execution time estimation experiment. (corresponds to table 5 & 6 in the paper)
#for each benchmark, 1st: run without speculative execution (-specu=0)
#                    2nd: run with speculative execution, and just-in-time merging option (-specu=1 -merge=0)
#                    3rd: run with speculative execution, and roll-back merging option (-specu=1 -merge=1)

time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/wcet/adpcm.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/wcet/adpcm.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=1 bench/wcet/adpcm.bc -o /dev/null

time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/mibench/susan.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/mibench/susan.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=1 bench/mibench/susan.bc -o /dev/null

time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/mibench/layer3.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/mibench/layer3.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=1 bench/mibench/layer3.bc -o /dev/null

time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/mibench/jcmarker.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/mibench/jcmarker.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=1 bench/mibench/jcmarker.bc -o /dev/null

time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/mibench/jdmarker.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/mibench/jdmarker.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=1 bench/mibench/jdmarker.bc -o /dev/null

time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/mibench/jcphuff.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/mibench/jcphuff.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=1 bench/mibench/jcphuff.bc -o /dev/null

time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/mibench/gtk.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/mibench/gtk.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=1 bench/mibench/gtk.bc -o /dev/null

time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/media/g72x.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/media/g72x.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=1 bench/media/g72x.bc -o /dev/null

time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/media/vga.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/media/vga.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=1 bench/media/vga.bc -o /dev/null

time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/media/stc.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/media/stc.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=1 bench/media/stc.bc -o /dev/null
