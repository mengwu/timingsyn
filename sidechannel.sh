#testing script for side channel detection experiment. (corresponds to table 7 in the paper)
#for each benchmark, 1st: run without speculative execution (-specu=0)
#                    2nd: run with speculative execution (-specu=1)

echo "hpn-ssh/hash:"
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/hpn-ssh/hash.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/hpn-ssh/hash.bc -o /dev/null
echo ""
echo "tomcrypt/encode"
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/tomcrypt/encode.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/tomcrypt/encode.bc -o /dev/null
echo ""
echo "tomcrypt/chacha20"
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/tomcrypt/chacha20.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/tomcrypt/chacha20.bc -o /dev/null
echo ""
echo "tomcrypt/ocb"
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/tomcrypt/ocb.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/tomcrypt/ocb.bc -o /dev/null
echo ""
echo "tomcrypt/aes"
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/tomcrypt/aes.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/tomcrypt/aes.bc -o /dev/null
echo ""
echo "openssl/str2key"
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/openssl/str2key.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/openssl/str2key.bc -o /dev/null
echo ""
echo "openssl/des"
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/openssl/des.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/openssl/des.bc -o /dev/null
echo ""
echo "tegra/seed"
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/tegra/seed.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/tegra/seed.bc -o /dev/null
echo ""
echo "tegra/camellia"
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/tegra/camellia.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/tegra/camellia.bc -o /dev/null
echo ""
echo "tegra/salsa"
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=0 -merge=0 bench/tegra/salsa.bc -o /dev/null
time ~/llvmbuild/bin/opt -load ~/llvmbuild/lib/LLVMSpectre.so -spectre -specu=1 -merge=0 bench/tegra/salsa.bc -o /dev/null
